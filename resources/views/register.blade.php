<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" name="firstname" id="firstname"><br><br>

        <label for="lastname">Last name:</label><br><br>
        <input type="text" name="lastname" id="lastname"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" value="Male" name="gender" id="male">
        <label for="male">Male</label><br>
        <input type="radio" value="Female" name="gender" id="female">
        <label for="female">Female</label><br>
        <input type="radio" value="OtherGender" name="gender" id="othergender">
        <label for="othergender">Other</label><br><br>
        
        <label for="nationality">Nationality:</label> <br><br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
        <br><br>

        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox" value="bahasaindonesia" id="bahasaindonesia">
        <label for="bahasaindonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" value="english" id="english">
        <label for="english">English</label><br>
        <input type="checkbox" value="otherlanguage" id="otherlanguage">
        <label for="otherlanguage">Other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">


    </form>
</body>
</html>