<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str; 

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $firstName = Str::upper($request['firstname']);
        $lastName = Str::upper($request['lastname']);
        $namaLengkap = $firstName . " " . $lastName; 
        return view('welcome', ['namaLengkap' => $namaLengkap]);
    }
}
